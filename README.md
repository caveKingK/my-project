# my-project
分布式项目

1.创建项目
2.项目介绍：一个分布式的基础框架，提供可扩展的增删改查功能，清晰的模块分层
3.项目架构：spring+springmvc+mybatis,dubbo+zookeeper,elasticsearch大文本搜索功能
4.模块结构：
my-api:接口包
my-dao:dao模块
my-common:通用模块
my-service:业务实现模块
my-web:web层